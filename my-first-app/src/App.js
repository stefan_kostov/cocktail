import './App.css';
import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import  SearchList from "./components/Search"
import Favorites from './components/Favorites';
import View from './components/View';




function App() {
  
  return (
    <div>
    <BrowserRouter>
<Switch>
        <Redirect path="/" exact to="/search" />
        <Route exact path="/search" component= {SearchList} />
        <Route exact path="/view/:id" component= {View} />
        <Route exact path="/favorites" component= {Favorites} />
        
</Switch>
</BrowserRouter>
  
  
  </div>
  )};

export default App;


