export const include = (cocktail) => {
    return {
        type : 'INCLUDE',
        payload: cocktail
    };
};

export const remove = (cocktail) => { 
    return {
        type : 'REMOVE',
        payload: cocktail
    };
};