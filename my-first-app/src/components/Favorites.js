import "./Favorites.css";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";

import { useSelector, useDispatch } from "react-redux";
import { remove } from "../actions/reduxActions"

export const Favorites = () => {



  const history = useHistory();

  const [drinks, setDrinks] = useState([]);







  let resultArr = [];
  const [error, setError] = useState(null);

  const favorites = useSelector(state => state.Favorites);
  const dispatch = useDispatch()


  useEffect(() => {



    fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a`)
      .then(response => response.json())
      .then((data) => setDrinks(data))
      .then(_ => {


      })
      .catch((error) => setError(error.message));

  }, []);


  let j = 0
  if (drinks.drinks == undefined) {
    j = 0
  } else if (drinks.drinks.length >= 0) {
    j = drinks.drinks.length
  }




  for (let i = 0; i < j; i++) {
    if (drinks.drinks[i]) {
      resultArr.push(drinks.drinks[i])
    }
  }


  console.log(favorites);


  return (
    <div>
      <button style={{ fontSize: '30px' }} onClick={() => history.push(`/search`)}>Search more</button>
      {favorites.map((array) => (
        <div
          key={array.arr.idDrink}
          className="AllDrinks"
        >
          <div className="card" >
            <div className="card-body">
              <h5 className="title">{array.arr.strDrink}</h5>
              <img alt="thumbImg" src={array.arr.strDrinkThumb}></img>
              <button className='btn'
                onClick={() => {
                  const index = favorites.indexOf(array)
                  dispatch(remove(index))
                }} >Delete</button>


            </div>
          </div>

        </div>


      ))}


    </div>
  );
};

export default Favorites;


