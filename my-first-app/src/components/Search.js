import "./Search.css";
import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router";

import { useSelector, useDispatch } from "react-redux";
import { include } from "../actions/reduxActions";




export const SearchList = () => {


  const history = useHistory();

  const [drinks, setDrinks] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  


  let resultArr = [];
  const [error, setError] = useState(null);


  const favorites = useSelector(state => state.Favorites);
  const dispatch = useDispatch()



  useEffect(() => {



    fetch(`https://thecocktaildb.com/api/json/v1/1/search.php?s=${query}`)
      .then(response => response.json())
      .then((data) => setDrinks(data))
      .then(_ => {


      })
      .catch((error) => setError(error.message));

  }, [searchTerm]);


  const handleOnInputChange = (e) => {
    const query = e.target.value;
    setSearchTerm(query)
  }

  const query = searchTerm;



  let j = 0
  if (drinks.drinks == undefined) {
    j = 0
  } else if (drinks.drinks.length < 3) {
    j = drinks.drinks.length
  }
  else { j = 3 }



  for (let i = 0; i < j; i++) {
    if (drinks.drinks[i]) {
      resultArr.push(drinks.drinks[i])
    }
  }


  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }


  return (
    <div>

      <input type="search" rows='2' cols='100' placeholder='What are you looking for..' value={query} onChange={handleOnInputChange} />
      <button className='btn1' onClick={() => history.push(`/favorites`)}>View Favorites</button>
      <br></br>
{query? 
<div>
      {resultArr.map((arr) => (
        <div
          key={arr.idDrink}
          className="AllDrinks"
        >

          <div className="card" >
            <div className="card-body">
              <h5 className="title" >{arr.strDrink}</h5>
              <img alt="thumbImg" src={arr.strDrinkThumb}></img>
              <h5 className="cardIng">Main ingridient : {arr.strIngredient1}</h5>

              <button className='btn1' onClick={() => {
                history.push(`/view/${arr.idDrink}`)
              }
              } >View</button>
              <button className='btn1' onClick={() => dispatch(include({ arr }))}>Add</button>
             
            </div>
          </div>

        </div>
      ))}
      
</div>
: <div></div>
}

    </div>
  );
};

export default SearchList;




