import "./View.css";

import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import Loader from "../Loader";




export const View = (props) => {
  const { id } = props.match.params;

  const history = useHistory();

  const [drink, setDrink] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  let result = [];


  useEffect(() => {

    setLoading(true);
    fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`)
      .then(response => response.json())
      .then((data) => setDrink(data))
      .then(_ => {
console.log(drink);

      })
      .catch((error) => setError(error.message))

      .finally(() => setLoading(false));
  }, []);


  result.concat(drink)
  console.log(result);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i> An error has occured: </i>
        {error}
      </h4>
    );
  }


  return (
    <div>

      <div  >
        <button className='btn' style={{ fontSize: '15px' }} onClick={() => history.push(`/search`)}>Search more</button>
        <div>{drink.drinks[0].strDrink}</div>
        <div>{drink.drinks[0].strInstructions}</div>
        <div>{drink.drinks[0].strAlcoholic}</div>

        <img style={{ margin: '15px' }} src={drink.drinks[0].strDrinkThumb}></img>
      </div>

    </div>
  );
};

export default View;
